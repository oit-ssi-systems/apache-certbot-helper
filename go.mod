module gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper

go 1.16

require (
	github.com/ghodss/yaml v1.0.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	go.hein.dev/go-version v0.1.0
	gopkg.in/ini.v1 v1.51.0
)
