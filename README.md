
[![pipeline status](https://gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper/-/commits/main) 
 [![coverage report](https://gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper/badges/main/coverage.svg)](https://gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper/-/commits/main) 
# apache-certbot-helper

Replaces ye' olden python script

Download a package from the [Release
Page](https://gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper/-/releases)
for quick and easy testing. Make sure it's on yum-01 when you want to deploy it
everywhere

## Running manually

Check if cert needs to be updated with the `check` command
```
apache-certbot-helper check $path_to_cert_yaml
```

Do the actual update
```
apache-certbot-helper run $path_to_cert_yaml
```

## Run in Puppet

Add the following to your hiera file:

```yaml
p_apache::certbot_helper: 'go'
```