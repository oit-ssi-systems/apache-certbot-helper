package cmd

import (
	"fmt"
	"io/ioutil"

	"github.com/ghodss/yaml"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper/helpers"
	"gopkg.in/ini.v1"
)

// runCmd represents the run command
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Run certbot",
	Long:  `Run certbot and get a new cert, based on the p_apache bits in PCS`,
	Example: `# Force renew
$ apache-certbot-helper run /etc/duke/certificate_meta/foo-01.oit.duke.edu.yaml -f
# Dry run
$ apache-certbot-helper run /etc/duke/certificate_meta/foo-01.oit.duke.edu.yaml -d`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("run called")

		// Collect CLI flags
		force, err := cmd.Flags().GetBool("force")
		CheckErr(err)
		dryrun, err := cmd.Flags().GetBool("dry-run")
		CheckErr(err)

		// Extract domains from YAML
		yamlConfig := args[0]
		mainDomain, err := helpers.ExtractDomain(yamlConfig)
		CheckErr(err)
		log.Debug("Main domain is: ", mainDomain)

		// Detect if apache is already running
		apacheRunning := helpers.IsPortOpen(80)
		log.Debug("Apache running is: ", apacheRunning)

		// Look up additional domains
		var extraDomains []string
		yamlFile, err := ioutil.ReadFile(yamlConfig)
		if err != nil {
			log.Printf("yamlFile.Get err   #%v ", err)
		}
		err = yaml.Unmarshal(yamlFile, &extraDomains)
		CheckErr(err)

		// Run the actual certbot command
		config := &helpers.CertbotRunConfig{
			PrimaryDomain: mainDomain,
			ExtraDomains:  extraDomains,
			ApacheRunning: apacheRunning,
			Force:         force,
			DryRun:        dryrun,
		}
		err = helpers.RunCertbot(config)
		CheckErr(err)

		// Set Renewal params if apache isn't running
		if !apacheRunning {
			renewalConfigFile := fmt.Sprintf("/etc/letsencrypt/renewal/%s.conf", mainDomain)
			log.Debugln("Renewal config file: ", renewalConfigFile)
			cfg, err := ini.Load(renewalConfigFile)
			CheckErr(err)
			cfg.Section("renewalparams").Key("authenticator").SetValue("webroot")
			cfg.Section("renewalparams").Key("webroot_path").SetValue("/var/lib/letsencrypt/webroot,")
			cfg.Section("renewalparams").Key("renew_hook").SetValue("/sbin/apachectl graceful")

			// Write the renewal file
			if !dryrun {
				log.Println("Saving renewal info to: ", renewalConfigFile)
				cfg.SaveTo(renewalConfigFile)
			}

		}

	},
}

func init() {
	rootCmd.AddCommand(runCmd)
	runCmd.Flags().BoolP("force", "f", false, "Force renewal, even if not yet due")
	runCmd.Flags().BoolP("dry-run", "d", false, "Only do a dry run of the command")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// runCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// runCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
