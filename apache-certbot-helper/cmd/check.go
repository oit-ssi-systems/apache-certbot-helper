/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	"reflect"
	"sort"

	"github.com/ghodss/yaml"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper/helpers"
)

// checkCmd represents the check command
var checkCmd = &cobra.Command{
	Use:   "check",
	Short: "Check if cerbot should be run",
	Args:  cobra.ExactArgs(1),
	Long:  `Look at the domains in the p_apache yaml file, and compare them against the domains in the actual certificate.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("check called")

		// Extract domains from YAML
		yamlConfig := args[0]
		mainDomain, err := helpers.ExtractDomain(yamlConfig)
		CheckErr(err)
		log.Debug("Main domain is: ", mainDomain)

		// Look up additional domains
		var extraDomains []string
		yamlFile, err := ioutil.ReadFile(yamlConfig)
		if err != nil {
			log.Printf("yamlFile.Get err   #%v ", err)
		}
		err = yaml.Unmarshal(yamlFile, &extraDomains)
		CheckErr(err)
		extraDomains = append(extraDomains, mainDomain)

		// Load up main actual cert file
		mainCertFile := fmt.Sprintf("/etc/letsencrypt/live/%s/fullchain.pem", mainDomain)
		_, err = os.Stat(mainCertFile)
		CheckErr(err)
		certData, err := ioutil.ReadFile(mainCertFile)
		CheckErr(err)

		block, _ := pem.Decode([]byte(certData))
		if block == nil {
			panic("failed to parse certificate PEM")
		}

		cert, err := x509.ParseCertificate(block.Bytes)
		if err != nil {
			panic("failed to parse certificate: " + err.Error())
		}

		// Clean up domain listings
		sort.Strings(extraDomains)
		certDomains := cert.DNSNames
		sort.Strings(cert.DNSNames)

		if !reflect.DeepEqual(certDomains, extraDomains) {
			log.Fatalf("Certificate altnames do not match. Wanted '%s' but got '%s'", extraDomains, certDomains)
		} else {
			log.Println("Cert is all good!")
		}

	},
}

func init() {
	rootCmd.AddCommand(checkCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// checkCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// checkCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
