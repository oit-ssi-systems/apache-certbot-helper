package helpers_test

import (
	"fmt"
	"net"
	"testing"

	"gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper/helpers"
)

func TestIfPortOpen(t *testing.T) {
	listenPort := 4242
	fakePort := 4243
	ln, _ := net.Listen("tcp", fmt.Sprintf(":%v", listenPort))
	defer ln.Close()
	if !helpers.IsPortOpen(listenPort) {
		t.Fatalf("Port was not detected as open")
	}

	if helpers.IsPortOpen(fakePort) {
		t.Fatalf("Detected fake port as open")
	}

}
