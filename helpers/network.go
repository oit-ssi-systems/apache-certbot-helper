package helpers

import (
	"net"
	"strconv"
	"time"
)

func IsPortOpen(port int) bool {
	timeout := time.Second
	conn, err := net.DialTimeout("tcp", net.JoinHostPort("localhost", strconv.Itoa(port)), timeout)
	var ret bool = false
	if err != nil {
		ret = false
	} else if conn != nil {
		defer conn.Close()
		ret = true
	}
	return ret
}
