package helpers

import (
	"fmt"
	"log"
	"path/filepath"
	"strings"
)

func ExtractDomain(path string) (string, error) {
	stripExt := strings.TrimSuffix(path, filepath.Ext(path))
	_, domain := filepath.Split(stripExt)
	if !strings.Contains(domain, ".") {
		return "", fmt.Errorf("InvalidDomainExtracted")
	}
	log.Println(domain)

	return domain, nil

}
