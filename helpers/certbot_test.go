package helpers_test

import (
	"testing"

	"gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper/helpers"
)

func TestCertbotRun(t *testing.T) {

	// Test Dry run
	c := &helpers.CertbotRunConfig{DryRun: true}
	err := helpers.RunCertbot(c)
	if err != nil {
		t.Fatalf("Could not dry-run vanilla certbot config")
	}

	c = &helpers.CertbotRunConfig{DryRun: false, CertbotBin: "true"}
	err = helpers.RunCertbot(c)
	if err != nil {
		t.Fatalf("Could not run vanilla certbot config")
	}

}
