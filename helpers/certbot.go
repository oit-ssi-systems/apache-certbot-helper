package helpers

import (
	"os/exec"

	log "github.com/sirupsen/logrus"
)

type CertbotRunConfig struct {
	PrimaryDomain string
	ExtraDomains  []string
	ApacheRunning bool
	Force         bool
	DryRun        bool
	CertbotBin    string
}

func RunCertbot(c *CertbotRunConfig) error {

	var args []string
	if c.ApacheRunning {
		args = []string{"certonly", "--webroot", "-w", "/var/lib/letsencrypt/webroot", "--deploy-hook", "/sbin/apachectl graceful"}
	} else {
		args = []string{"certonly", "--standalone"}
	}
	args = append(args, "-n",
		"-m", "csi-linux-admins@duke.edu",
		"--no-eff-email", "--agree-tos",
		"--server", "https://locksmith.oit.duke.edu/acme/v2/directory",
		"-d", c.PrimaryDomain)

	if c.Force {
		args = append(args, "--force-renewal")
	}
	for _, extraDomain := range c.ExtraDomains {
		args = append(args, "-d", extraDomain)

	}

	if !c.DryRun {
		// Allow for certbot binary to be passed in the config object
		var realCertbotBin string
		if c.CertbotBin == "" {
			realCertbotBin = "certbot"
		} else {
			realCertbotBin = c.CertbotBin
		}

		log.Println("Executing certbot with these arguments: ", args)
		cmd := exec.Command(realCertbotBin, args...)
		err := cmd.Run()
		if err != nil {
			return err
		}
	} else {
		log.Println("Would have run certbot with these args, if not dry running: ", args)
	}

	return nil

}
