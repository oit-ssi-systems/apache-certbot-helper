package helpers_test

import (
	"testing"

	"gitlab.oit.duke.edu/oit-ssi-systems/apache-certbot-helper/helpers"
)

func TestValidConfigPathDomain(t *testing.T) {
	got, _ := helpers.ExtractDomain("/tmp/whatever.foo.com.yaml")
	want := "whatever.foo.com"

	if got != want {
		t.Fatalf("Did not extra proper domain from configpath, got: '%s'", got)
	}

}

func TestInValidConfigPathDomain(t *testing.T) {
	_, err := helpers.ExtractDomain("foo")
	if err == nil {
		t.Fatalf("Did not get an error when attemping to extract a bad domain")
	}

}
